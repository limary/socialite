<?php
namespace Sinta\Socialite;

interface FactoryInterface
{
    /**
     * Get an OAuth provider implementation.
     *
     * @param string $driver
     *
     * @return \Sinta\Socialite\ProviderInterface
     */
    public function driver($driver);
}
