<?php
namespace Sinta\Socialite;

interface UserInterface
{
    public function getId();

    public function getNickname();

    public function getName();

    public function getEmail();

    public function getAvatar();
}