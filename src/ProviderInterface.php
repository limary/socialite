<?php
namespace Sinta\Socialite;

interface ProviderInterface
{
    public function redirect();


    public function user(AccessTokenInterface $token = null);
}
