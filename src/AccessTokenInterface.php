<?php
namespace Sinta\Socialite;

interface AccessTokenInterface
{
    /**
     * accesstoken
     * @return string
     */
    public function getToken();
}
